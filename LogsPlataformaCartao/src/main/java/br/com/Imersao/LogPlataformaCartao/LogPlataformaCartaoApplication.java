package br.com.Imersao.LogPlataformaCartao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class LogPlataformaCartaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogPlataformaCartaoApplication.class, args);
	}

}
