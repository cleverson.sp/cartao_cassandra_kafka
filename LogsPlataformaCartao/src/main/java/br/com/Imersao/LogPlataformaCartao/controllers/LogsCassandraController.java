package br.com.Imersao.LogPlataformaCartao.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.LogPlataformaCartao.models.LogsCassandra;
import br.com.Imersao.LogPlataformaCartao.services.LogPlataformaCartaoService;

@RestController
@RequestMapping("/logs")
public class LogsCassandraController {
	
	@Autowired
	LogPlataformaCartaoService  logPlataformaCartaoService;
	
	@GetMapping
	public Iterable<LogsCassandra> consultaLogs(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date hora){
		
		return logPlataformaCartaoService.consultaLogs(hora);
	}
	
	
	

}