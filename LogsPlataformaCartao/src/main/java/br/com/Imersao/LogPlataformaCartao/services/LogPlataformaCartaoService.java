package br.com.Imersao.LogPlataformaCartao.services;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.Imersao.LogPlataformaCartao.models.LogsCassandra;
import br.com.Imersao.LogPlataformaCartao.repositories.LogsRepository;

@Component
public class LogPlataformaCartaoService {

	@Autowired
	LogsRepository logsRepository;

	@KafkaListener(id = "appcartaoconsumer", topics = "Cleverson_logs_1")
	public void logs(@Payload String str) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();

		LogsCassandra logs = mapper.readValue(str, LogsCassandra.class);

		logs.setMsg("Cliente Cadastrado com sucesso");

		logsRepository.save(logs);

	}

	public Iterable<LogsCassandra> consultaLogs(Date hora) {
		
		
		Calendar calendar =  Calendar.getInstance();
		calendar.setTime(hora);
		calendar.add(Calendar.DATE, +1);
		Date dataFinal = calendar.getTime();
		return logsRepository.findbyAllData(hora, dataFinal);

	}

}
