package br.com.Imersao.LogPlataformaCartao.repositories;

import java.util.Date;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.LogPlataformaCartao.models.LogsCassandra;

public interface LogsRepository extends CrudRepository<LogsCassandra, String>{
	
	@Query("SELECT * FROM logscassandra where servico = 'Cliente' and hora >= :hora and hora<=:dataFinal" )
	public Iterable<LogsCassandra> findbyAllData(Date hora, Date dataFinal);

}
