package br.com.Imersao.LogPlataformaCartao.models;

import java.util.Date;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class LogsCassandra {

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String servico;

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	// @JsonFormat(pattern = "DD/MM/YY")
	private Date hora;

	private String dadosCliente;
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getServico() {
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public String getDadosCliente() {
		return dadosCliente;
	}

	public void setDadosCliente(String dadosCliente) {
		this.dadosCliente = dadosCliente;
	}

}
