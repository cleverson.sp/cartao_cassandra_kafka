package br.com.Imersao.login.services;

import java.util.Collections;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.Imersao.login.models.Login;
import br.com.Imersao.login.repositories.LoginRepository;

@Service
public class LoginService implements UserDetailsService  {

	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	 private Logger logger = LoggerFactory.getLogger(this.getClass());

	public Login criar(String cpf, String senha) {
		
		 logger.info("Criando usuário CPF -->" + cpf);
		
		Login login = new Login();
		
		login.setCpf(cpf);
		login.setSenha(passwordEncoder.encode(senha));

		return loginRepository.save(login);
	}
 
	  @Override
	  public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException{
		logger.info("buscanco CPF do usuário -->" + cpf);
		  Optional<Login> optional = loginRepository.findByCpf(cpf);
	    
	    if(!optional.isPresent()) {
	      throw new UsernameNotFoundException("Usuário não encontrado");
	    }
	    
	    Login login = optional.get();
	    
	    SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");
	    
	    return new User(login.getCpf(), login.getSenha(), Collections.singletonList(authority));
	  }

}
