package br.com.Imersao.login.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.login.models.Login;

public interface LoginRepository extends CrudRepository<Login, Integer>{
	
	Optional<Login> findByCpf(String cpf);

}
