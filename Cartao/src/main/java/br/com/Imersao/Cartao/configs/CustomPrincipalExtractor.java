package br.com.Imersao.Cartao.configs;

import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import br.com.Imersao.Cartao.models.Cartao;

public class CustomPrincipalExtractor implements PrincipalExtractor {

	@Override
	public Object extractPrincipal(Map<String, Object> map) {
		Cartao cartao = new Cartao();
		cartao.setIdCliente((String) map.get("name"));

		return cartao;
	}

}
