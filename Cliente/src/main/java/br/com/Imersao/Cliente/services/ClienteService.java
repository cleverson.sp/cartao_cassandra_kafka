package br.com.Imersao.Cliente.services;

import java.util.Date;
import java.util.Optional;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.Imersao.Cliente.dtos.Logs;
import br.com.Imersao.Cliente.models.Cliente;
import br.com.Imersao.Cliente.repositories.ClienteRepository;
import br.com.Imersao.Cliente.repositories.LoginClient;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private LoginClient LoginClient;

	@Autowired
	private KafkaTemplate<String, String> template;

	public Cliente criar(Cliente cliente, String cpf, String senha) throws Exception {

		cliente.setId(cpf);

		LoginClient.criar(cpf, senha);

		ObjectMapper mapper = new ObjectMapper();
		Logs logs = new Logs();
		
		logs.setDadosCliente(mapper.writeValueAsString(cliente));

		logs.setHora(new Date());
		logs.setServico("Cliente");
	
		template.send(new ProducerRecord<String, String>("Cleverson_logs_1","1", mapper.writeValueAsString(logs)));

		return clienteRepository.save(cliente);
	}

	public Optional<Cliente> buscar(String id) {
		return clienteRepository.findById(id);
	}

}
