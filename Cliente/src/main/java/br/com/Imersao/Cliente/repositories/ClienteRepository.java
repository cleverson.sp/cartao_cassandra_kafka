package br.com.Imersao.Cliente.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.Cliente.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, String>{
  
}
