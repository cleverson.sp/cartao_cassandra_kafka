package br.com.Imersao.Cliente.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Cliente {
  @Id
  private String id;
  @NotBlank
  private String nome;

  public String getNome() {
    return nome;
  }

  public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public void setNome(String nome) {
    this.nome = nome;
  }

//  public String getCpf() {
//    return cpf;
//  }
//
//  public void setCpf(String cpf) {
//    this.cpf = cpf;
//  }
//
//  public String getSenha() {
//    return senha;
//  }
//
//  public void setSenha(String senha) {
//    this.senha = senha;
//  }
}
