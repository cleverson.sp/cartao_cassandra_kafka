package br.com.Imersao.Cliente.repositories;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.Imersao.Cliente.dtos.Login;

@FeignClient(name = "Login")
public interface LoginClient {
	
	@PostMapping("/login/criar")
	public Optional<Login> criar(@RequestParam String cpf, @RequestParam  String senha);
	
}
