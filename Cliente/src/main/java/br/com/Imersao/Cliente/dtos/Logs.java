package br.com.Imersao.Cliente.dtos;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Logs {

	private String servico;
	//@JsonFormat(pattern = "DD/MM/YY")
	@JsonSerialize
	private Date hora;
	private String dadosCliente;

	public String getServico() {
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public String getDadosCliente() {
		return dadosCliente;
	}

	public void setDadosCliente(String dadosCliente) {
		this.dadosCliente = dadosCliente;
	}

}
